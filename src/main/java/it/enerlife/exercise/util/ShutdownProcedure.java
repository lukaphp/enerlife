package it.enerlife.exercise.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ShutdownProcedure  {

  @Autowired
  private ApplicationContext ctx;

	public void off() {
		((ConfigurableApplicationContext) ctx).close();
		System.exit(1);

	}
}
