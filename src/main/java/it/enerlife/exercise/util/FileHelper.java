package it.enerlife.exercise.util;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileHelper {

	 public static File stream2file (byte[] byteArray, String filename) throws IOException {
	        final File tempFile = File.createTempFile(filename,".pdf",null);
	        FileOutputStream fos = new FileOutputStream(tempFile);
	        fos.write(byteArray);
	        fos.close();
	        return tempFile;
	    }

}
