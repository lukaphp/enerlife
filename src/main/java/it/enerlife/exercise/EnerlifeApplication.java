package it.enerlife.exercise;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.testng.reporters.jq.BannerPanel;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class EnerlifeApplication {

    public static void main(String[] args) {
//        SpringApplication.run(WebSignatureRecognitionApplication.class, args);
        SpringApplication app = new SpringApplication(EnerlifeApplication.class); 
//        app.setShowBanner(true);
        app.run(args);
    }
}
