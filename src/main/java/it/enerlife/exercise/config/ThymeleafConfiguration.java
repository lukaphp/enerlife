package it.enerlife.exercise.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.annotation.PostConstruct;
 

@Configuration
public class ThymeleafConfiguration {

	 @Autowired
	    private SpringTemplateEngine templateEngine;

	    @PostConstruct
	    public void extension() {
	        FileTemplateResolver resolver = new FileTemplateResolver();
	        resolver.setPrefix("src/main/resources/static/app/");	        
	        resolver.setSuffix(".html");
	        resolver.setTemplateMode("HTML5");
	        resolver.setOrder(templateEngine.getTemplateResolvers().size());
	        resolver.setCacheable(false);
	        templateEngine.addTemplateResolver(resolver);
	    }
	    
	    
}
