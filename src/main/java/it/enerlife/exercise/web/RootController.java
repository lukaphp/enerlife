package it.enerlife.exercise.web;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import it.enerlife.exercise.model.Accredito;
import it.enerlife.exercise.model.Timetable;
import it.enerlife.exercise.repository.AccreditoRepository;
import it.enerlife.exercise.repository.TimetableRepository;
import it.enerlife.exercise.util.ShutdownProcedure;



@Controller
class RootController {

	private final Logger _logger = LoggerFactory.getLogger(RootController.class);

	@Autowired
	private AccreditoRepository accreditoRepository;
	@Autowired
	private TimetableRepository timetableRepository;

    @Value("${info.app.version:0.0.0}")
    private String version;

    @Autowired
    private ShutdownProcedure shutdownProc;

    private static Long  draftId;

    @PersistenceContext
    private EntityManager em;

    @RequestMapping("/")
    String index(HttpServletRequest httpRequest, Model model) {
        model.addAttribute("version", version);
        model.addAttribute("counter",null);
        return "index";
    }

    @RequestMapping(value = "/idle")
    public String waiting(Model model) {
        model.addAttribute("counter",null);
        return "index";
    }

    @RequestMapping(value = "/progress")
    public @ResponseBody Long progressMonitor(Model model) {
        Long progress = null;
        return progress;
    }


//    @RequestMapping("/shutdown")
//    String shutdown() {
//    	shutdownProc.off();
//        return "index";
//    }


    @RequestMapping(value="/p", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @Transactional
    @RequestMapping(value = "/first-upload", method = RequestMethod.POST)
    public @ResponseBody String firstUpload(@RequestParam("file") MultipartFile file) throws ParseException{

    	 String fileName = file.getOriginalFilename();

    	 BufferedReader br = null;
         String line = "";
         String cvsSplitBy = ";";

         try {

             //truncate accredditi table
             CriteriaBuilder builder = em.getCriteriaBuilder();
             CriteriaDelete<Accredito> query = builder.createCriteriaDelete(Accredito.class);
             query.from(Accredito.class);
             em.createQuery(query).executeUpdate();

             br = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
             while ((line = br.readLine()) != null) {

                 String[] lines = line.split(cvsSplitBy);

                 Accredito accredito = new  Accredito();
                 accredito.setDescrizione(lines[0]);

                 accredito.setImporto(Double.valueOf(lines[1]));

                 // YYYY-MM-DDThh:mm:ssTZD
                 ZonedDateTime result = ZonedDateTime.parse(lines[2], DateTimeFormatter.ISO_DATE_TIME);

                 accredito.setDataOra(result.toInstant().toEpochMilli());

                 accredito.setTransazioni(Integer.parseInt(lines[3]));

                 accreditoRepository.save(accredito);

             }

         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             if (br != null) {
                 try {
                     br.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             }
         }

 		return "index";
    }

    @Transactional
    @RequestMapping(value = "/second-upload", method = RequestMethod.POST)
    public @ResponseBody String secondUpload(@RequestParam("file") MultipartFile file) throws ParseException{

    	 String fileName = file.getOriginalFilename();

    	 BufferedReader br = null;
         String line = "";
         String cvsSplitBy = ";";

         try {

             //truncate accredditi table
             CriteriaBuilder builder = em.getCriteriaBuilder();
             CriteriaDelete<Timetable> query = builder.createCriteriaDelete(Timetable.class);
             query.from(Timetable.class);
             em.createQuery(query).executeUpdate();

             br = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
             while ((line = br.readLine()) != null) {

                 String[] lines = line.split(cvsSplitBy);

                 Timetable timetable = new  Timetable();

                 // YYYY-MM-DDThh:mm:ssTZD
                 ZonedDateTime result = ZonedDateTime.parse(lines[0], DateTimeFormatter.ISO_DATE_TIME);

                 timetable.setDataOra(result.toInstant().toEpochMilli());
                 timetableRepository.save(timetable);

             }

         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             if (br != null) {
                 try {
                     br.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             }
         }

 		return "index";
    }


    @RequestMapping(value = "/process", method = RequestMethod.GET)
	public @ResponseBody ReportDto process(){
		_logger.debug("Start Processing");
		ReportDto reportDto = new ReportDto();

		//recupero lista orari dal secondo file
		List<Timetable> timetableList = timetableRepository.findAll();
		Double totImporti = 0D;
		Integer totTransazioni = 0;
		Boolean exactMatch = false;
		List<Accredito> accreditiList = accreditoRepository.findAll();
		for (Timetable timetable : timetableList) {
			exactMatch = false;
			Iterator<Accredito> iter = accreditiList.iterator();

			while (iter.hasNext()) {
				Accredito accredito = iter.next();
				if(accredito.getDataOra() == timetable.getDataOra()) {
					_logger.info("data ora: "+accredito.getDataOra());
					_logger.info("importo: "+accredito.getImporto());
					_logger.info("transazioni: "+accredito.getTransazioni());
					totImporti += accredito.getImporto();
					totTransazioni += accredito.getTransazioni();
					reportDto.setImporto(totImporti);
					reportDto.setTransazioni(totTransazioni);
					reportDto.getAccreditiList().add(accredito);
					exactMatch = true;
					break;
				}
			}

			if(!exactMatch) {
				Iterator<Accredito> iter2 = accreditiList.iterator();
				while (iter2.hasNext()) {
					Accredito accredito = iter2.next();
					if(timetable.getDataOra() < accredito.getDataOra()) {
						_logger.info("data ora: "+accredito.getDataOra());
						_logger.info("importo: "+accredito.getImporto());
						_logger.info("transazioni: "+accredito.getTransazioni());
						totImporti += accredito.getImporto();
						totTransazioni += accredito.getTransazioni();
						reportDto.setImporto(totImporti);
						reportDto.setTransazioni(totTransazioni);
						reportDto.getAccreditiList().add(accredito);
						break;
					}
				}
			}


		}

		return reportDto;



    }


}