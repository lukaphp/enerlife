package it.enerlife.exercise.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.enerlife.exercise.model.Accredito;


public class ReportDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double importo;
	private Integer transazioni;

	private List<Accredito> accreditiList;


	public ReportDto() {
		super();
		this.importo = 0D;
		this.transazioni = 0;
		this.accreditiList = new ArrayList<Accredito>();
	}

	public Double getImporto() {
		return importo;
	}
	public void setImporto(Double importo) {
		this.importo = importo;
	}
	public Integer getTransazioni() {
		return transazioni;
	}
	public void setTransazioni(Integer transazioni) {
		this.transazioni = transazioni;
	}

	public List<Accredito> getAccreditiList() {
		return accreditiList;
	}

	public void setAccreditiList(List<Accredito> accreditiList) {
		this.accreditiList = accreditiList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((importo == null) ? 0 : importo.hashCode());
		result = prime * result + ((transazioni == null) ? 0 : transazioni.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ReportDto))
			return false;
		ReportDto other = (ReportDto) obj;
		if (importo == null) {
			if (other.importo != null)
				return false;
		} else if (!importo.equals(other.importo))
			return false;
		if (transazioni == null) {
			if (other.transazioni != null)
				return false;
		} else if (!transazioni.equals(other.transazioni))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ReportDto [importo=" + importo + ", transazioni=" + transazioni + "]";
	}




}
