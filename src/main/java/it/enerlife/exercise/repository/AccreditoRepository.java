package it.enerlife.exercise.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.enerlife.exercise.model.Accredito;


public interface AccreditoRepository extends JpaRepository<Accredito, Long> {

	@Query("select d from Accredito d where d.dataOra >= ?1")
	Page<Accredito> findAccrediti(long dataOra, Pageable pageable);


}
