package it.enerlife.exercise.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import it.enerlife.exercise.model.Accredito;
import it.enerlife.exercise.model.Timetable;


public interface TimetableRepository extends JpaRepository<Timetable, Long> {


}
