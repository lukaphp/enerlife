package it.enerlife.exercise.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TIMETABLE")
public class Timetable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DATAORA")
	private long  dataOra;

	public long getDataOra() {
		return dataOra;
	}

	public void setDataOra(long dataOra) {
		this.dataOra = dataOra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (dataOra ^ (dataOra >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Timetable))
			return false;
		Timetable other = (Timetable) obj;
		if (dataOra != other.dataOra)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Timetable [dataOra=" + dataOra + "]";
	}



}
