package it.enerlife.exercise.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.LocalDateTime;

@Entity
@Table(name = "ACCREDITI")
public class Accredito implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(name="DESCRIZIONE")
	private String descrizione;

	@Column(name="IMPORTO")
	private Double importo;

	@Column(name = "DATAORA")
	private long dataOra;

	@Column(name="TRANSAZIONI")
	private Integer transazioni;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescrizione() {
		return descrizione;
	}


	public Double getImporto() {
		return importo;
	}

	public void setImporto(Double importo) {
		this.importo = importo;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public long getDataOra() {
		return dataOra;
	}

	public void setDataOra(long dataOra) {
		this.dataOra = dataOra;
	}

	public Integer getTransazioni() {
		return transazioni;
	}

	public void setTransazioni(Integer transazioni) {
		this.transazioni = transazioni;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Accredito))
			return false;
		Accredito other = (Accredito) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Accredito [id=" + id + ", descrizione=" + descrizione + ", importo=" + importo + ", dataOra=" + dataOra
				+ ", transazioni=" + transazioni + "]";
	}

}
