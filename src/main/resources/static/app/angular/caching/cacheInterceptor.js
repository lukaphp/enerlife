﻿'use strict';

angular.module('WsrApplication.Angular.Caching', ['WsrApplication.Config', 'WsrApplication.Common'])
    .factory('SmartCacheInterceptor', ['$q', 'WsrApplicationVersion', function ($q, WsrApplicationVersion) {
        return {
            request: function (config) {
                if (config.url.indexOf(".htm") > -1) {
                    var separator = config.url.indexOf("?") === -1 ? "?" : "&";
                    config.url = config.url + separator + "v=" + WsrApplicationVersion;
                }
                return config || $q.when(config);
            }
        };
    }]);
;