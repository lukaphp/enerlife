﻿'use strict';

/*
This module
*/
angular.module('WsrApplication.Angular.Data', ['ngResource'])
    .controller('DatasController', ['$scope', '$resource','$http', function ($scope, $resource,$http) {
        var clients = $resource("api/clients");

        $scope.reload = function () {
            $scope.data = clients.query();
        };

        $scope.reload();
    }])
    .controller('AppController', function($scope, $resource, FileUploader) {

    	var content= [];
    	var showReport = false;
    	$scope.firstFile;



    	var processApi = $resource("/process",{
            'get': { method: 'GET' }
    	});


    	$scope.firstUploader = new FileUploader({
            autoUpload: true,
            url: '/first-upload'
        });

		$scope.firstUploader.onAfterAddingFile = function(fileItem) {
	         console.info('onAfterAddingFirstFile', fileItem);
	         $scope.firstFile = fileItem;
	         $scope.isFirstUploaded=true;
	    };


	    $scope.firstUploader.onErrorItem = function(item, response, status, headers) {
	    	console.log('response',response);
	    	if(response.status != 200){
	    		 alert('Formato file non valido !')
	    		 $scope.firstFile = null;
	        	 $scope.isFirstUploaded=false;
	    	}
	    }

	    $scope.secondUploader = new FileUploader({
            autoUpload: true,
            url: '/second-upload'
        });

		$scope.secondUploader.onAfterAddingFile = function(fileItem) {
	         console.info('onAfterAddingSecondFile', fileItem);
	         $scope.secondFile = fileItem;
	         $scope.isSecondUploaded=true;
	    };

	    $scope.secondUploader.onErrorItem = function(item, response, status, headers) {
	    	console.log('response',response);
	    	if(response.status != 200){
	    		 alert('Formato file non valido !')
	    		 $scope.secondFile = null;
	        	 $scope.isSecondUploaded=false;
	    	}
	    }


        $scope.processBtn = function() {

		  	 processApi.get().$promise
    		  	.then(function(data){
    		  	 showReport = true;
    		  	 $scope.data = data;
       		  	 console.log(data)
    		  	});

        	}

        $scope.formatLocalDate = function(dataOra) {
            var now = new Date(dataOra),
                tzo = -now.getTimezoneOffset(),
                dif = tzo >= 0 ? '+' : '-',
                pad = function(num) {
                    var norm = Math.abs(Math.floor(num));
                    return (norm < 10 ? '0' : '') + norm;
                };
//                tzo = tzo/2;
            return now.getFullYear()
                + '-' + pad(now.getMonth()+1)
                + '-' + pad(now.getDate())
                + 'T' + pad(now.getHours())
                + ':' + pad(now.getMinutes())
                + ':' + pad(now.getSeconds())
                + dif + pad(tzo / 60)
                + ':' + pad(tzo % 60);
        }




    })

