﻿'use strict';

/*
This is an Angular module, which will represent our application
*/
angular.module('WsrApplication', [
    'ngResource',
    'ngRoute',
    'ui.date',
    'ui.bootstrap.tabs',
    'ui.bootstrap.dropdownToggle',
    'WsrApplication.Angular.Caching',
    'WsrApplication.Common',
    'WsrApplication.Config',
    'WsrApplication.Angular.Data',
    'WsrApplication.Angular.Loader',
    'angularFileUpload',
    'angular-ladda','ngCookies','pdf','radialIndicator'
]).
config(['$routeProvider', '$provide', '$httpProvider', function ($routeProvider, $provide, $httpProvider) {

    $routeProvider.when(
    '/main', {
        templateUrl: '/app/index.html',
        controller: 'AppController'
    });

    $routeProvider.when(
    	    '/template', {
    	        templateUrl: '/app/template.html',
    	        controller: 'AppController'
    	    });

    $routeProvider.when(
    	    '/idle', {
    	    	templateUrl: '/app/idle.html',
    	        controller: 'PollingController'
    	    });





    $routeProvider.when(
        '/angular/caching', {
            templateUrl: '/app/angular/caching/index.html'
    });

    $routeProvider.when(
        '/angular/data/remote', {
            templateUrl: '/app/angular/data/index.html',
            controller: 'DatasController'
    });

    $routeProvider.when(
       '/angular/data/remote/:id', {
           templateUrl: '/app/angular/data/detail.html',
           controller: 'DataController'
    });

    $routeProvider.when(
        '/angular/loader', {
            templateUrl: '/app/angular/loader/index.html',
            controller: 'LoaderController'
        });

    $routeProvider.when(
        '/angular/directives', {
            templateUrl: '/app/angular/directives/index.html',
            controller: 'DirectivesController'
    });

    $routeProvider.when(
        '/spring-boot/filters', {
            templateUrl: '/app/spring-boot/filters.html',
            controller: ['$scope', '$http', function ($scope, $http) {
                $scope.get = function (url) {
                    $http.get(url);
                };

                $scope.post = function (url, data) {
                    $http.post(url, data);
                };
            }]
    });

    $routeProvider.otherwise({
        redirectTo: '/main'
    });

    $httpProvider.interceptors.push('SmartCacheInterceptor');
}])
;