﻿'use strict';

angular.module('WsrApplication.Common', ['ngResource'])
    .directive('sampleMenu', [function () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: '/app/common/menu.html'
        }
    }])
    .directive('showSource', ['$resource', function ($resource) {
        return {
            templateUrl: '/app/common/source.html',
            restrict: 'EA',
            replace: true,
            scope: {
            },
            link: function (scope, elem, attrs) {
                var res = $resource('/api/source');

                scope.file = res.get({ path : attrs.url });
            }
        }
    }])
    .directive('modal', function () {
	    return {
	      template: '<div class="modal fade">' + 
	          '<div class="modal-dialog">' + 
	            '<div class="modal-content">' + 
	              '<div class="modal-header">' + 
	                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
	                '<h4 class="modal-title">{{ title }}</h4>' + 
	              '</div>' + 
	              '<div class="modal-body" ng-transclude></div>' + 
	            '</div>' + 
	          '</div>' + 
	        '</div>',
	      restrict: 'E',
	      transclude: true,
	      replace:true,
	      scope:true,
	      link: function postLink(scope, element, attrs) {
	        scope.title = attrs.title;
	
	        scope.$watch(attrs.visible, function(value){
	          if(value == true)
	            $(element).modal('show');
	          else
	            $(element).modal('hide');
	        });
	
	        $(element).on('shown.bs.modal', function(){
	          scope.$apply(function(){
	            scope.$parent[attrs.visible] = true;
	          });
	        });
	
	        $(element).on('hidden.bs.modal', function(){
	          scope.$apply(function(){
	            scope.$parent[attrs.visible] = false;
	          });
	        });
	      }
	    };
  })
  
.directive('myPdfViewerToolbar', [
     'pdfDelegate',
  function(pdfDelegate) {
    return {
       restrict: 'E',
       template:
        '<div class="clearfix mb2 white bg-blue">' +
          '<div class="left">' +
            '<a href=""' +
              'ng-click="prev()"' +
              'class="button py2 m0 button-nav-dark">Back' +
             '</a>' +
            '<a href=""' +
              'ng-click="next()"' +
              'class="button py2 m0 button-nav-dark">Next' +
            '</a>' +
            '<span class="px1">Page</span> ' +
            '<input type="text" class="field-dark" ' +
              'min=1 ng-model="currentPage" ng-change="goToPage()" ' +
               'style="width: 10%"> ' +
            ' / {{pageCount}}' +
          '</div>' +
        '</div>',
       scope: { pageCount: '=' },
       link: function(scope, element, attrs) {
         var id = attrs.delegateHandle;
         scope.currentPage = 1;

        scope.prev = function() {
          pdfDelegate
            .$getByHandle(id)
            .prev();
          updateCurrentPage();
        };
        scope.next = function() {
          pdfDelegate
            .$getByHandle(id)
            .next();
          updateCurrentPage();
        };
        scope.goToPage = function() {
          pdfDelegate
             .$getByHandle(id)
             .goToPage(scope.currentPage);
         };

        var updateCurrentPage = function() {
          scope.currentPage = pdfDelegate
                                .$getByHandle(id)
                                 .getCurrentPage();
        };
      }
    };
}]);